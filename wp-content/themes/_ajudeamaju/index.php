<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package ajudeamaju
 */
global $post;
get_header();
?>
	<div class="pg pg-inicial">
		<section class="bannerDestaque">
			<button class="carrosselDestaqueEsquerda"></button>
			<button class="carrosselDestaqueDireita"></button>
			<?php 
				//LOOP DE DESTAQUES
				$postsDestaques = new WP_Query(array(
					'post_type'     => 'destaque',
					'posts_per_page'   => -1,
					)
				);
			?>
			<h6 class="hidden">Banner Destaque</h6>
			<div class="bannerDestaqueInicial" id="bannerDestaqueInicial">
			<?php while($postsDestaques->have_posts()): 
				$postsDestaques->the_post();
				global $post;
				//FOTO DESTACADA
				$fotoPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
				$fotoPost = $fotoPost[0];

			?>
				<div class="item">
					<a href="#">
						<figure>
							<img src="<?php echo $fotoPost; ?>" alt="<?php echo get_the_title(); ?>">
						</figure>
					</a>
				</div>
			<?php endwhile; wp_reset_query(); ?>
			</div>
		</section>
		<div class="containerFull">
			<div class="row">
				<div class="col-md-9">
					<section class="postsInicial">
						<?php if(have_posts()): 
							while(have_posts()):
								the_post();
								
								$categorias = get_the_category();
								$idCategoria;
								
								foreach ($categorias as $categorias){
									$nomeCategoria = $categorias->name;
									$idCategoria   = $categorias->cat_ID;
									//$bannerCategoria     = z_taxonomy_image_url(get_queried_object()->cat_ID);
									$separaItem    = explode("|", $categorias->description);
									$corCategoria  = $separaItem[0];
									$iconeCategoria= $separaItem[1];
								}
								
								if(!$corCategoria){
									$corCategoria = "#44E7CC";
								}
								if(!$iconeCategoria){
									$iconeCategoria = get_template_directory_uri() . '/img/iconeCategory.png';
								}

								$imagemDestaquePost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
								$imagemDestaquePost = $imagemDestaquePost[0];
								
								$titulo = get_the_title();
								$link = get_permalink();

						?>
						<div class="post">
							<div class="row">
								<div class="texto">
									<div class="titulo">
										<a href="<?php echo $link; ?>">
											<h2><?php echo $titulo; ?></h2>
										</a>
									</div>
									<div class="conteudo">
										<p><?php customExcerpt(200); ?></p>
									</div>
									<div class="categoria" style="border-bottom: 3px solid <?php echo $corCategoria; ?>;">
										<a href="<?php echo get_category_link( $idCategoria ) ?>" style="color: <?php echo $corCategoria; ?>;"><img src="<?php echo $iconeCategoria ?>" alt=""><?php echo $nomeCategoria; ?></a>
									</div>
								</div>
								<div class="imagem">
									<div class="data">
										<span style="color: <?php echo $corCategoria; ?>;"><?php echo get_the_date('d/m/Y '); ?></span>
										<ul>
											<li>
												<img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="">
											</li>
											<li>
												<img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.png" alt="">
											</li>
											<li>
												<img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="">
											</li>
											<li>
												<img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="">
											</li>
										</ul>
									</div>
									<figure>
										<img src="<?php echo $imagemDestaquePost ?>" alt="<?php echo $titulo; ?>">
									</figure>
									<a href="<?php echo $link; ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/seta.png" alt="<?php echo $titulo; ?>" class="btn-entrar"></a>
								</div>
							</div>
						</div>
						<?php endwhile; endif; ?>
						<div class="ver-mais">
							<button>VER MAIS</button>
						</div>
					</section>
				</div>
				<div class="col-md-3">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</div>

<?php

get_footer();
