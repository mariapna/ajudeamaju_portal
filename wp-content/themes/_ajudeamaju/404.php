<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package ajudeamaju
 */

get_header();
?>

	<div class="pg-404">
		<div class="logo">
		</div>
		<div class="conteudo">
			<p>Página não encontrada.</p>
			<a href="<?php echo get_home_url() .'/blog'; ?>">Continue Navegando.</a>
		</div>
	</div>
<?php
get_footer();
